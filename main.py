# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Joshua Trees <joshua.trees@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gitlab
from durations import Duration
import re


#############################################################################
## CONFIG ###################################################################
#############################################################################

hostname = 'https://gitlab.com'
access_token = '<insert_here>'

# A gitlab username or `None`.
filter_by_author = None

# A string with the following format: "YYYY-MM-DD", or `None`.
filter_by_date_begin = None
filter_by_date_end   = None


#############################################################################
## FUNCTIONS ################################################################
#############################################################################

def fetch_time_entries(gl, filter_by_author=None, filter_by_date_begin=None, filter_by_date_end=None):
  time_entries = []

  # Iterate through projects and fetch their issues because directly fetching
  # issues only only returns issues created by the token owner.
  projects = gl.projects.list()
  for project in projects:
    issues = project.issues.list(all=True)

    for issue in issues:
      p_issue = gl.projects.get(issue.project_id, lazy=True).issues.get(issue.iid, lazy=True)
      notes = p_issue.notes.list(all=True)

      for note in notes:
        if note.system and 'time spent' in note.body:
          date_str = parse_date(note.body)
          duration = parse_duration(note.body)

          # Skip if specified filters result in a mismatch.
          if filter_by_author and note.author['username'] != filter_by_author:
            continue
          if filter_by_date_begin and date_str < filter_by_date_begin:
            continue
          if filter_by_date_end and date_str > filter_by_date_end:
            continue

          # Add a time_entry object to the result.
          time_entries.append({ 'date': date_str, 'issue_iid': issue.iid, 'duration': duration })

  return time_entries

def format_duration(duration_in_s, tabular=False):
  hours, remainder = divmod(duration_in_s, 3600)
  minutes, seconds = divmod(remainder, 60)

  if tabular:
    return '%sh %2dm' %(hours, minutes)
  else:
    return '%sh %sm' %(hours, minutes)

def format_issue_numbers(numbers):
  return ', '.join(["#%s" %(str(n)) for n in sorted([*numbers])])

def parse_date(str):
  return re.search(r'(?<=at )\d{4}-\d{2}-\d{2}$', str).group()

def parse_duration(str):
  duration_str = re.search(r'^(?:added|subtracted) (.*) of time spent', str).group(1)
  # TODO: Parse "1d" as 8h instead of 24h.
  duration = int(Duration(duration_str).to_seconds())

  if re.match(r'^subtracted', str):
    duration = -duration

  return duration

def report(entries, by='entry'):
  if by == 'entry':
    for entry in entries:
      print('%s | %s | %s' %(entry['date'], entry['issue_iid'], format_duration(entry['duration'])))
  elif by == 'date':
    time_spent_per_day = {}

    # Group entries by date.
    total = 0
    for entry in entries:
      date      = entry['date']
      duration  = entry['duration']
      issue_iid = entry['issue_iid']

      # Might as well do this here.
      total += duration

      if date in time_spent_per_day:
        time_spent_per_day[date]['duration'] += duration
        time_spent_per_day[date]['issues'].add(issue_iid)
      else:
        time_spent_per_day[date] = { 'duration': duration, 'issues': { issue_iid } }

    sorted_dates = sorted([*time_spent_per_day])

    # Prepare the rows and calculate how wide the issues column will have to
    # be for everything to fit in a fancy table.
    issue_column_width = 6
    for date in sorted_dates:
      issues = time_spent_per_day[date]['issues']
      issue_numbers = format_issue_numbers(issues)

      if len(issue_numbers) > issue_column_width:
        issue_column_width = len(issue_numbers)

    print('| Date       | Time Spent | %s |' %('Issues'.ljust(issue_column_width)))
    print('|------------|------------|-%s-|' %('-' * issue_column_width))

    for date in sorted_dates:
      entry = time_spent_per_day[date]
      duration = entry['duration']
      issues = time_spent_per_day[date]['issues']

      f_duration = format_duration(duration, tabular=True).rjust(10)
      f_issue_numbers = format_issue_numbers(issues).ljust(issue_column_width)

      print('| %s | %s | %s |' %(date, f_duration, f_issue_numbers))

    print('|------------|------------|-%s-|' %('-' * issue_column_width))
    print('| Total      | %s | %s |' %(format_duration(total, tabular=True).rjust(10), ' ' * issue_column_width))
  else:
    raise ArgumentError('Unspported value of `by`: %s' %(by))


#############################################################################
## ENTRY POINT ##############################################################
#############################################################################

gl = gitlab.Gitlab(hostname, private_token=access_token)

time_entries = fetch_time_entries(gl, filter_by_author=filter_by_author, filter_by_date_begin=filter_by_date_begin, filter_by_date_end=filter_by_date_end)
report(time_entries, by='date')
