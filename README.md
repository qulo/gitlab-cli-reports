# gitlab-cli-reports


## About

gitlab-cli-reports is a command line tool for creating timesheet reports via GitLab.


## Setup

Ensure you have the following dependencies installed.

* python-3.5
* pipenv

Then run the following:

```sh
pipenv install
```


## Running

```sh
pipenv run python3 main.py
```
